using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using memes.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace memes.Repositories
{
    public interface INoteRepository
    {
        void Save(Note note);
        void Update(Note note);
        void Delete(int id);
        Note Find(int id);
        IEnumerable<Note> FindAll();
    }
}